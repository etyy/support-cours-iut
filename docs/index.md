---
hide:
  - navigation
  - toc
---
# Welcome

<figure markdown>
  [:fontawesome-solid-paper-plane:](mailto:etienne.loutsch@unicaen.fr){ .md-button .md-button--primary .center }
  [:fontawesome-brands-linkedin-in:](https://www.linkedin.com/in/etienneloutsch/){ .md-button .md-button--primary center }  

  Build with [MkDocs](https://squidfunk.github.io/mkdocs-material/) on Gitlab [Pages](https://docs.gitlab.com/ee/user/project/pages/)
</figure>
